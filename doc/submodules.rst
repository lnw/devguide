

Working with Git submodules
===========================

The Dalton repository includes other repositories
via Git submodules (which, in turn, may include
other repositories).

Therefore we recommend to clone the repository with the ``--recursive`` flag::

  $ git clone --recursive https://gitlab.com/dalton/dalton.git

When switching Dalton branches remember to update the references::

  $ git submodule update --init --recursive
