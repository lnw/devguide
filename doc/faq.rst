

F.A.Q.
======


CMake Error: The source directory X does not appear to contain CMakeLists.txt
-----------------------------------------------------------------------------

You have probably cloned without ``--recursive``. To remedy this you
do not have to clone again, simply run::

  $ git submodule update --init --recursive


How do I know whether I need to run git submodule update after I pull or switch branches?
-----------------------------------------------------------------------------------------

To see this run ``git status``. If you then see external modules which you haven't
touched as modified, then you can update them using::

  $ git submodule update --init --recursive

Note that switching branches or pulling updates does not automatically
update the submodules.


I have some problem with gitlab.com. Is this my problem or a problem on gitlab.com side?
----------------------------------------------------------------------------------------

To see whether gitlab.com has some known issue, please check:

- http://status.gitlab.com
- https://twitter.com/gitlabstatus
- https://gitlab.com/gitlab-org/gitlab-ce/issues


Do I need to create a ssh keypair on each and every machine I want to use Dalton?
---------------------------------------------------------------------------------

No. You do not need to authenticate via ssh. You can clone via https::

  $ git clone https://gitlab.com/dalton/dalton.git

It will ask you for your gitlab.com user and password.
This is useful when you need to clone to a machine just once in a while (cluster)
and where you don't do heavy development work.
